/* eslint-disable @typescript-eslint/no-var-requires,object-shorthand */
const path = require('path');
const rules = require('./webpack.rules');
const plugins = require('./webpack.plugins');

rules.push({
  test: /\.css$/,
  use: [{ loader: 'style-loader' }, { loader: 'css-loader' }],
});

module.exports = {
  // Put your normal webpack config below here
  module: {
    rules,
  },
  resolve: {
    extensions: ['.js', '.ts', '.jsx', '.tsx'],
    alias: {
      Common: path.resolve(__dirname, '../src/renderer/common'),
      Pages: path.resolve(__dirname, '../src/renderer/pages'),
      Services: path.resolve(__dirname, '../src/renderer/services'),
      Utils: path.resolve(__dirname, '../src/renderer/utils'),
      Theme: path.resolve(__dirname, '../src/renderer/theme'),
      UI: path.resolve(__dirname, '../src/renderer/ui'),
    },
  },
  plugins: plugins,
};
