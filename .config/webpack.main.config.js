/* eslint-disable global-require,@typescript-eslint/no-var-requires,object-shorthand */
const path = require('path');
const plugins = require('./webpack.plugins');

module.exports = {
  /**
   * This is the main entry point for your application, it's the first file
   * that runs in the main process.
   */
  entry: path.resolve(__dirname, '../src/main/main.ts'),
  // Put your normal webpack config below here
  module: {
    rules: require('./webpack.rules'),
  },
  resolve: {
    extensions: ['.js', '.ts', '.jsx', '.tsx'],
    alias: {
      Common: path.resolve(__dirname, '../src/renderer/common'),
      Pages: path.resolve(__dirname, '../src/renderer/pages'),
      Services: path.resolve(__dirname, '../src/renderer/services'),
      Utils: path.resolve(__dirname, '../src/renderer/utils'),
      Theme: path.resolve(__dirname, '../src/renderer/theme'),
      UI: path.resolve(__dirname, '../src/renderer/ui'),
    },
  },
  plugins: plugins,
};
