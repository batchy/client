/* eslint-disable @typescript-eslint/no-var-requires,import/no-extraneous-dependencies */
const path = require('path');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = [
  new ForkTsCheckerWebpackPlugin({
    async: false,
  }),
  new CopyWebpackPlugin([
    {
      from: path.resolve(__dirname, '../src/renderer/assets'),
      to: path.resolve(__dirname, '../.webpack/renderer/assets'),
    },
  ]),
];
