import { URI } from 'Utils/URI';

// SignUp
// Method: POST
// FormData: { username, password }
// returns a token.
export const SignUp = data => {
  return fetch(`${URI}/public/register`, {
    method: 'POST',
    body: data,
  });
};

// SignIn
// Method: POST
// FormData: { username, password }
// returns userData and a token.
export const SignIn = data => {
  return fetch(`${URI}/public/login`, {
    method: 'POST',
    body: data,
  });
};
