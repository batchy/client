import { URI } from 'Utils/URI';

export const GetBatches = token => {
  return fetch(`${URI}/restricted/batch`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const AddBatch = (token, data) => {
  return fetch(`${URI}/restricted/batch`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};

export const UpdateBatch = (token, data) => {
  return fetch(`${URI}/restricted/batch`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};

export const DeleteBatch = (token, data) => {
  return fetch(`${URI}/restricted/batch`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};
