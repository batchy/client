import { parse } from 'url';
import { remote } from 'electron';
import qs from 'qs';
import axios from 'axios';
import appStorage from 'Utils/AppStorage';

const GOOGLE_AUTHORIZATION_URL = 'https://accounts.google.com/o/oauth2/v2/auth';
const GOOGLE_TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token';
const GOOGLE_PROFILE_URL = 'https://www.googleapis.com/userinfo/v2/me';
const GOOGLE_CLIENT_ID = '561636613588-s021u9if7e0hjtd7muedvs73la1qv4to.apps.googleusercontent.com';
const GOOGLE_REDIRECT_URI = 'com.batcher.app:redirect_uri_path';

// AccessTokens describes an object Google returns when an access code is sent to GOOGLE_TOKEN_URL.
interface AccessTokens {
  access_token: string;
  expires_in: number;
  refresh_token: string;
  scope: string[];
  token_type: string;
  id_token: string;
}

// UserProvider describes an object Google return when a token is sent to GOOGLE_PROFILE_URL.
interface UserProvider {
  id: string;
  email: string;
  verified_email: boolean;
  name: string;
  given_name: string;
  family_name: string;
  picture: string;
  locale: string;
}

// UserProfile describes an object that is returned upon a successful sign-in.
interface UserProfile {
  uid: string;
  email: string;
  displayName: string;
  idToken: string;
}

export const signInWithPopup = (): Promise<string> => {
  return new Promise<string>((resolve, reject) => {
    const authWindow = new remote.BrowserWindow({
      width: 500,
      height: 600,
      show: true,
    });

    const urlParams = {
      response_type: 'code',
      redirect_uri: GOOGLE_REDIRECT_URI,
      client_id: GOOGLE_CLIENT_ID,
      scope: 'profile email',
    };
    const authUrl = `${GOOGLE_AUTHORIZATION_URL}?${qs.stringify(urlParams)}`;

    const handleNavigation = (url: string): void => {
      const { query } = parse(url, true);
      if (query) {
        if (query.error) {
          reject(new Error(`There was an error: ${query.error}`));
        } else if (query.code) {
          // Login is complete
          authWindow.removeAllListeners('closed');
          setImmediate(() => authWindow.close());
          // This is the authorization code we need to request tokens
          resolve(query.code as string);
        }
      }
    };

    authWindow.on('closed', () => {
      reject(new Error('Auth window was closed by user'));
    });

    authWindow.webContents.on('will-navigate', (event, url) => {
      handleNavigation(url);
    });

    authWindow.loadURL(authUrl);
  });
};

export const fetchAccessTokens = async (code: string): Promise<AccessTokens> => {
  const response = await axios.post(
    GOOGLE_TOKEN_URL as string,
    qs.stringify({
      code,
      client_id: GOOGLE_CLIENT_ID,
      redirect_uri: GOOGLE_REDIRECT_URI,
      grant_type: 'authorization_code',
    }),
    {
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
  );
  return response.data;
};

export const fetchGoogleProfile = async (accessToken: string): Promise<UserProvider> => {
  const response = await axios.get(GOOGLE_PROFILE_URL as string, {
    headers: {
      'Content-Type': 'application/json',
      Authorization: `Bearer ${accessToken}`,
    },
  });
  return response.data;
};

export const googleSignIn = async (): Promise<void> => {
  try {
    const code: string = await signInWithPopup();
    const tokens: AccessTokens = await fetchAccessTokens(code);
    const { id, email, name } = await fetchGoogleProfile(tokens.access_token);
    await appStorage.setItem('userData', {
      uid: id,
      email,
      displayName: name,
      idToken: tokens.id_token,
    } as UserProfile);
  } catch (err) {
    throw new Error(err.message);
  }
};
