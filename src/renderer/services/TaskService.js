import { URI } from 'Utils/URI';

export const DeleteSubtask = (token, data) => {
  return fetch(`${URI}/restricted/subtask`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};

export const CreateSubtask = (token, data, id) => {
  return fetch(`${URI}/restricted/task/${id}/subtask`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};

export const CreateTask = (token, data) => {
  return fetch(`${URI}/restricted/task`, {
    method: 'POST',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};

export const UpdateTask = (token, data) => {
  return fetch(`${URI}/restricted/task`, {
    method: 'PUT',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};

export const DeleteTask = (token, data) => {
  return fetch(`${URI}/restricted/task`, {
    method: 'DELETE',
    headers: {
      Authorization: `Bearer ${token}`,
    },
    body: data,
  });
};

export const GetCurrentTasks = token => {
  return fetch(`${URI}/restricted/tasks/current`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};

export const GetCompletedTasks = token => {
  return fetch(`${URI}/restricted/tasks/completed`, {
    method: 'GET',
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
};
