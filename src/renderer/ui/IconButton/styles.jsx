import styled from 'styled-components';
import { StyledIcon } from 'UI/Icon/styles';

export const StyledIconButton = styled.div`
  display: flex;
  width: ${props => props.width};
  height: ${props => props.height};
  justify-content: center;
  align-items: center;

  background: transparent;
  border-radius: 100%;
  &:hover {
    background: ${props =>
      (props.variant === 'primary' && props.theme.colors.c_Accent_hover) ||
      (props.variant === 'success' && props.theme.colors.c_Success_hover) ||
      (props.variant === 'danger' && props.theme.colors.c_Danger_hover)};
  }
  &:active {
    background: ${props =>
      (props.variant === 'primary' && props.theme.colors.c_Accent_active) ||
      (props.variant === 'success' && props.theme.colors.c_Success_active) ||
      (props.variant === 'danger' && props.theme.colors.c_Danger_active)};
  }

  ${StyledIcon} {
    fill: ${props =>
      (props.variant === 'primary' && props.theme.colors.c_Accent) ||
      (props.variant === 'success' && props.theme.colors.c_Success) ||
      (props.variant === 'danger' && props.theme.colors.c_Danger)};
  }
`;
