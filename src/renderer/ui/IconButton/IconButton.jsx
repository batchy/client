import React from 'react';
import * as PropTypes from 'prop-types';
import Icon from 'UI/Icon/Icon';
import { StyledIconButton } from './styles';

const IconButton = props => {
  const { variant, src, width, height, onClick } = props;
  return (
    <StyledIconButton variant={variant} width={width} height={height} onClick={onClick}>
      <Icon src={src} width={width} height={height} />
    </StyledIconButton>
  );
};

IconButton.propTypes = {
  variant: PropTypes.string,
  src: PropTypes.element.isRequired,
  width: PropTypes.string,
  height: PropTypes.string,
  onClick: PropTypes.func,
};

IconButton.defaultProps = {
  variant: 'primary',
  width: '24px',
  height: '24px',
  onClick: () => {},
};

export default IconButton;
