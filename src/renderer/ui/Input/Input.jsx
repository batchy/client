import React from 'react';
import * as PropTypes from 'prop-types';
import Typography from '../Typography/Typography';
import { StyledInput, StyledInputContainer } from './styles';

const Input = props => {
  const { height, width, fullWidth, defaultValue, label, onChange, required, name, type, value } = props;

  return (
    <StyledInputContainer fullWidth={fullWidth}>
      {label && (
        <Typography variant="p" fontFamily="regular" fontSize="20px" htmlFor={name}>
          {label}
        </Typography>
      )}
      <StyledInput
        value={value}
        height={height}
        width={width}
        defaultValue={defaultValue}
        onChange={onChange}
        required={required}
        name={name}
        type={type}
      />
    </StyledInputContainer>
  );
};

Input.propTypes = {
  height: PropTypes.string,
  width: PropTypes.string,
  fullWidth: PropTypes.bool,
  defaultValue: PropTypes.string,
  label: PropTypes.string,
  onChange: PropTypes.func,
  required: PropTypes.bool,
  name: PropTypes.string,
  type: PropTypes.string,
  value: PropTypes.string.isRequired,
};

Input.defaultProps = {
  height: '',
  width: '',
  fullWidth: false,
  defaultValue: '',
  label: '',
  onChange: () => {},
  required: false,
  name: '',
  type: 'text',
};

export default Input;
