import styled from 'styled-components';

export const StyledInputContainer = styled.div`
  display: flex;
  flex-flow: column nowrap;
  width: ${props => (props.fullWidth ? '100%' : 'auto')};
`;

export const StyledInput = styled.input`
  height: ${props => (props.height ? props.height : 'auto')};
  min-height: 36px;
  width: ${props => (props.width ? props.width : 'auto')};

  padding: ${props => props.theme.offsets.xs};

  border: 3px solid ${props => props.theme.colors.c_Accent_active};
  border-radius: 5px;
  font-family: ${props => props.theme.fonts.regular};
  font-size: 16px;

  outline: none;
  transition: border 0.2s ease;
  will-change: border;

  &:focus {
    border: 3px solid ${props => props.theme.colors.c_Accent};
  }
`;
