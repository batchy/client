import React from 'react';
import * as PropTypes from 'prop-types';
import { StyledButton } from './styles';

const Button = props => {
  const { onClick, variant, children } = props;
  return (
    <StyledButton variant={variant} onClick={onClick}>
      {children}
    </StyledButton>
  );
};

Button.propTypes = {
  onClick: PropTypes.func,
  variant: PropTypes.string,
  children: PropTypes.string.isRequired,
};

Button.defaultProps = {
  onClick: () => {},
  variant: 'primary',
};

export default Button;
