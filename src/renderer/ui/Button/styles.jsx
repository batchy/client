import styled from 'styled-components';

export const StyledButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 4px 8px;
  border: 2px solid
    ${props =>
      (props.variant === 'primary' && props.theme.colors.c_Accent) ||
      (props.variant === 'success' && props.theme.colors.c_Success) ||
      (props.variant === 'danger' && props.theme.colors.c_Danger) ||
      (props.variant === 'secondary' && props.theme.colors.c_Primary)};
  background: transparent;
  border-radius: 5px;
  color: ${props =>
    (props.variant === 'primary' && props.theme.colors.c_Accent) ||
    (props.variant === 'success' && props.theme.colors.c_Success) ||
    (props.variant === 'danger' && props.theme.colors.c_Danger) ||
    (props.variant === 'secondary' && props.theme.colors.c_Primary)};
  font-size: 16px;
  font-family: ${props => props.theme.fonts.bold};
  outline: none;
  cursor: pointer;
  transition: background 0.2s, color 0.2s ease;
  will-change: background, color;

  &:hover {
    background: ${props =>
      (props.variant === 'primary' && props.theme.colors.c_Accent_hover) ||
      (props.variant === 'success' && props.theme.colors.c_Success_hover) ||
      (props.variant === 'danger' && props.theme.colors.c_Danger_hover) ||
      (props.variant === 'secondary' && props.theme.colors.c_Primary_hover)};
  }

  &:active {
    background: ${props =>
      (props.variant === 'primary' && props.theme.colors.c_Accent_active) ||
      (props.variant === 'success' && props.theme.colors.c_Success_active) ||
      (props.variant === 'danger' && props.theme.colors.c_Danger_active) ||
      (props.variant === 'secondary' && props.theme.colors.c_Primary_active)};
  }
`;
