import styled from 'styled-components';

export const StyledIcon = styled.div`
  width: ${props => props.width};
  height: ${props => props.height};
  fill: ${props => (props.theme.colors[props.fill] ? props.theme.colors[props.fill] : props.fill)};
`;
