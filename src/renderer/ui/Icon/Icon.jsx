import React from 'react';
import * as PropTypes from 'prop-types';
import { StyledIcon } from './styles';

const Icon = props => {
  const { src, fill, width, height, className } = props;
  return (
    <StyledIcon className={className} fill={fill} width={width} height={height}>
      {src}
    </StyledIcon>
  );
};

Icon.defaultProps = {
  className: '',
  fill: 'c_Accent',
  width: '24px',
  height: '24px',
};

Icon.propTypes = {
  className: PropTypes.string,
  src: PropTypes.element.isRequired,
  fill: PropTypes.string,
  width: PropTypes.string,
  height: PropTypes.string,
};

export default Icon;
