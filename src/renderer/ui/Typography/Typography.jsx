import { createElement } from 'react';
import * as PropTypes from 'prop-types';
import * as styledComponents from './styles';

const defaultFontMap = {
  h1: ['32px', 'bold'],
  h2: ['24px', 'bold'],
  h3: ['18px', 'bold'],
  h4: ['16px', 'bold'],
  h5: ['13px', 'bold'],
  h6: ['10px', 'bold'],
  p: ['16px', 'regular'],
  span: ['16px', 'regular'],
};

const Typography = props => {
  const { variant, fontSize, fontFamily, color, children, onClick } = props;
  // assign styled component to wrapper tag
  const wrapper = styledComponents[`${variant}Styled`];
  // select font size and font family, either default or overridden
  const wrapperFontSize = fontSize || defaultFontMap[variant][0];
  const wrapperFontFamily = fontFamily || defaultFontMap[variant][1];
  // generate element
  return createElement(wrapper, { fontSize: wrapperFontSize, fontFamily: wrapperFontFamily, color, onClick }, children);
};

Typography.propTypes = {
  variant: PropTypes.string,
  fontSize: PropTypes.string,
  fontFamily: PropTypes.string,
  color: PropTypes.string,
  children: PropTypes.string.isRequired,
  onClick: PropTypes.func,
};

Typography.defaultProps = {
  variant: 'p',
  color: 't_Primary',
  onClick: () => {},
};

export default Typography;
