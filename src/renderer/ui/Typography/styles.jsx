import styled from 'styled-components';

export const h1Styled = styled.h1`
  font-size: ${props => props.fontSize};
  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;

export const h2Styled = styled.h2`
  font-size: ${props => props.fontSize};
  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;

export const h3Styled = styled.h3`
  font-size: ${props => props.fontSize};
  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;

export const h4Styled = styled.h4`
  font-size: ${props => props.fontSize};
  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;

export const h5Styled = styled.h5`
  font-size: ${props => props.fontSize};
  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;

export const h6Styled = styled.h6`
  font-size: ${props => props.fontSize};

  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;

export const pStyled = styled.p`
  font-size: ${props => props.fontSize};

  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;

export const spanStyled = styled.span`
  font-size: ${props => props.fontSize};
  font-family: ${props => props.theme.fonts[props.fontFamily]};
  color: ${props => (props.theme.colors[props.color] ? props.theme.colors[props.color] : props.color)};
`;
