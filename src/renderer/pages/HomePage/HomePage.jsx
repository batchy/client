/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { useState } from 'react';
import Typography from 'UI/Typography/Typography';
import { Container, Headline } from './styles';
import SignUpModal from './components/SignUpModal/SignUpModal';
import SignInModal from './components/SignInModal/SignInModal';
import AuthActions from './components/AuthActions/AuthActions';

const HomePage = () => {
  const [signUpModalOpen, setSignUpModalOpen] = useState(false);
  const [signInModalOpen, setSignInModalOpen] = useState(false);
  return (
    <>
      <Container>
        <Headline>
          <Typography variant="h1" fontSize="128px" fontFamily="bold" color="t_Secondary">
            Batchy
          </Typography>
          <Typography variant="h2" fontSize="56px" fontFamily="bold" color="t_Secondary">
            Task management :)
          </Typography>
          <AuthActions setSignInModalOpen={setSignInModalOpen} setSignUpModalOpen={setSignUpModalOpen} />
        </Headline>
      </Container>

      <SignUpModal open={signUpModalOpen} onClose={() => setSignUpModalOpen(false)} />
      <SignInModal open={signInModalOpen} onClose={() => setSignInModalOpen(false)} />
    </>
  );
};

export default HomePage;
