import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { SignUp } from 'Services/AuthService';
import Typography from 'UI/Typography/Typography';
import IconButton from 'UI/IconButton/IconButton';
import { Close } from 'UI/Icons/Icons';
import Input from 'UI/Input/Input';
import Button from 'UI/Button/Button';
import { validateRequest } from 'Utils/validateRequest';
import appStorage from 'Utils/AppStorage';
import { useAppDispatch } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import { StyledModal, StyledModalActions, StyledModalTop, StyledOverlay } from './styles';

const SignUpModal = props => {
  const { open, onClose } = props;
  const dispatch = useAppDispatch();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async () => {
    try {
      const data = new FormData();
      data.append('username', username);
      data.append('password', password);

      await validateRequest(SignUp(data), result => {
        appStorage.setItem('token', result.token);
        dispatch({
          type: types.SET_USER_AUTH_STATUS,
          status: true,
        });
        window.location.hash = '#/tasks';
      });
    } catch (err) {
      alert(err.message);
    }
  };

  return (
    <StyledOverlay open={open}>
      <StyledModal>
        <StyledModalTop>
          <Typography color="c_Accent" variant="h2" fontFamily="bold">
            Sign up
          </Typography>
          <IconButton src={<Close />} height="24px" width="24px" variant="primary" onClick={onClose} />
        </StyledModalTop>
        <Input fullWidth required label="Username" onChange={e => setUsername(e.target.value)} />
        <Input fullWidth required type="password" label="Password" onChange={e => setPassword(e.target.value)} />
        <StyledModalActions>
          <Button variant="primary" onClick={handleSubmit}>
            Sign up
          </Button>
        </StyledModalActions>
      </StyledModal>
    </StyledOverlay>
  );
};

SignUpModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default SignUpModal;
