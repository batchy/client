import styled from 'styled-components';
import { StyledInputContainer } from 'UI/Input/styles';

export const StyledOverlay = styled.div`
  //positioning
  position: absolute;
  left: 0;
  top: 0;
  z-index: 1000;
  // dimensions
  width: 100vw;
  height: 100vh;

  display: flex;
  justify-content: center; // main axis
  align-items: center; // cross axis

  background: rgba(246, 246, 246, 0.6);
  visibility: ${props => (props.open ? 'visible' : 'hidden')};
  opacity: ${props => (props.open ? 1 : 0)};

  transition: 0.5s visibility, 0.5s opacity ease;
`;

export const StyledModal = styled.div`
  width: 400px;
  padding: ${props => props.theme.offsets.sm};

  display: flex;
  flex-flow: column nowrap;
  align-items: center;

  background: #ffffff;
  border: 2px solid ${props => props.theme.colors.c_Accent};
  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.2);
  box-sizing: border-box;
  border-radius: 8px;

  ${StyledInputContainer} {
    margin-bottom: ${props => props.theme.offsets.sm};
  }
`;

export const StyledModalTop = styled.div`
  width: 100%;
  height: 32px;

  margin-bottom: ${props => props.theme.offsets.sm};

  display: flex;
  flex-flow: row nowrap;
  justify-content: space-between;
  align-items: center;
`;

export const StyledModalActions = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
`;
