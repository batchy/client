import React, { useState } from 'react';
import * as PropTypes from 'prop-types';
import { SignIn } from 'Services/AuthService';
import Typography from 'UI/Typography/Typography';
import IconButton from 'UI/IconButton/IconButton';
import { Close } from 'UI/Icons/Icons';
import Input from 'UI/Input/Input';
import Button from 'UI/Button/Button';
import { validateRequest } from 'Utils/validateRequest';
import appStorage from 'Utils/AppStorage';
import { useAppDispatch } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import useLoader from 'Common/Loader/useLoader';
import { GetCompletedTasks, GetCurrentTasks } from 'Services/TaskService';
import { GetBatches } from 'Services/BatchService';
import { StyledModal, StyledModalActions, StyledModalTop, StyledOverlay } from './styles';

const SignInModal = props => {
  const { open, onClose } = props;
  const dispatch = useAppDispatch();
  const { setActive } = useLoader();

  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  const handleSubmit = async () => {
    setActive(true);
    try {
      const data = new FormData();
      data.append('username', username);
      data.append('password', password);

      await validateRequest(SignIn(data), async result => {
        appStorage.setItem('token', result.token);
        dispatch({
          type: types.SET_USER_AUTH_STATUS,
          status: true,
        });
        dispatch({
          type: types.SET_USER_DATA,
          data: result.data,
        });

        await validateRequest(GetCurrentTasks(result.token), result => {
          if (result === null) result = [];
          result.forEach(item => {
            if (item.subtasks === null) {
              item.subtasks = [];
            }
          });

          dispatch({
            type: types.SET_CURRENT_TASKS,
            data: result,
          });
        });

        await validateRequest(GetCompletedTasks(result.token), result => {
          if (result === null) result = [];
          result.forEach(item => {
            if (item.subtasks === null) {
              item.subtasks = [];
            }
          });
          dispatch({
            type: types.SET_COMPLETED_TASKS,
            data: result,
          });
        });

        await validateRequest(GetBatches(result.token), result => {
          if (result === null) result = [];
          result.forEach(batch => {
            if (batch.tasks === null) {
              batch.tasks = [];
            }
            batch.tasks.forEach(task => {
              if (task.subtasks === null) {
                task.subtasks = [];
              }
              task.batchName = batch.name;
              task.batchId = batch.id;
            });
          });
          dispatch({
            type: types.SET_BATCHES,
            data: result,
          });
        });

        window.location.hash = '#/tasks';
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <StyledOverlay open={open}>
      <StyledModal>
        <StyledModalTop>
          <Typography color="c_Accent" variant="h2" fontFamily="bold">
            Sign in
          </Typography>
          <IconButton src={<Close />} height="24px" width="24px" variant="primary" onClick={onClose} />
        </StyledModalTop>
        <Input fullWidth required label="Username" onChange={e => setUsername(e.target.value)} />
        <Input fullWidth required type="password" label="Password" onChange={e => setPassword(e.target.value)} />
        <StyledModalActions>
          <Button variant="primary" onClick={handleSubmit}>
            Sign in
          </Button>
        </StyledModalActions>
      </StyledModal>
    </StyledOverlay>
  );
};

SignInModal.propTypes = {
  open: PropTypes.bool.isRequired,
  onClose: PropTypes.func.isRequired,
};

export default SignInModal;
