import styled from 'styled-components';
import { StyledButton } from 'UI/Button/styles';

export const StyledAuthActions = styled.div`
  display: flex;
  flex-flow: row nowrap;
  justify-content: center;
  align-items: center;

  margin-top: ${props => props.theme.offsets.xs};

  ${StyledButton} {
    width: auto;
    height: 35px;
    margin-bottom: ${props => props.theme.offsets.xs};
    margin-right: ${props => props.theme.offsets.xs};
  }
`;
