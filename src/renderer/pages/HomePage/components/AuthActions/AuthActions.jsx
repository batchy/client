import React from 'react';
import * as PropTypes from 'prop-types';
import Button from 'UI/Button/Button';
import { StyledAuthActions } from './styles';

const AuthActions = props => {
  const { setSignInModalOpen, setSignUpModalOpen } = props;

  return (
    <StyledAuthActions>
      <Button variant="secondary" onClick={() => setSignUpModalOpen(true)}>
        Sign up
      </Button>
      <Button variant="secondary" onClick={() => setSignInModalOpen(true)}>
        Sign in
      </Button>
    </StyledAuthActions>
  );
};

AuthActions.propTypes = {
  setSignInModalOpen: PropTypes.func.isRequired,
  setSignUpModalOpen: PropTypes.func.isRequired,
};

export default AuthActions;
