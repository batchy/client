import styled from 'styled-components';
import { StyledButton } from 'UI/Button/styles';

export const Container = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
  background: ${props => props.theme.colors.c_Accent};
`;

export const Headline = styled.div`
  display: flex;
  flex-flow: column nowrap;
  align-items: center;
  text-align: center;
`;
