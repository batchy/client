import React, { useEffect, useState } from 'react';
import Input from 'UI/Input/Input';
import Button from 'UI/Button/Button';
import IconButton from 'UI/IconButton/IconButton';
import { Close } from 'UI/Icons/Icons';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import * as Modal from 'Common/Modal/styles';
import useLoader from 'Common/Loader/useLoader';
import appStorage from 'Utils/AppStorage';
import { validateRequest } from 'Utils/validateRequest';
import { AddBatch, GetBatches } from 'Services/BatchService';

const AddBatchModal = props => {
  const { addBatchModalOpen } = useAppState();
  const { setActive } = useLoader();
  const dispatch = useAppDispatch();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');

  const handleSubmit = async () => {
    setActive(true);
    const token = appStorage.getItem('token');
    const data = new FormData();
    data.append('name', name);
    data.append('description', description);
    try {
      await validateRequest(AddBatch(token, data), () => {
        dispatch({
          type: types.OPEN_ADD_BATCH_MODAL,
          status: false,
        });
      });
      await validateRequest(GetBatches(token), result => {
        if (result === null) result = [];
        result.forEach(batch => {
          if (batch.tasks === null) {
            batch.tasks = [];
          }
          batch.tasks.forEach(task => {
            if (task.subtasks === null) {
              task.subtasks = [];
            }
            task.batchName = batch.name;
            task.batchId = batch.id;
          });
        });
        setName('');
        setDescription('');
        dispatch({
          type: types.SET_BATCHES,
          data: result,
        });
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <Modal.Overlay open={addBatchModalOpen}>
      <Modal.Modal>
        <Modal.Title>
          <Modal.TitleText>Add a new batch</Modal.TitleText>
          <Modal.TitleExit>
            <IconButton
              src={<Close />}
              width="24px"
              height="24px"
              onClick={() =>
                dispatch({
                  type: types.OPEN_ADD_BATCH_MODAL,
                  status: false,
                })
              }
            />
          </Modal.TitleExit>
        </Modal.Title>

        <Modal.InputContainer>
          <Input
            value={name}
            label="Name"
            onChange={e => {
              setName(e.target.value);
            }}
          />
        </Modal.InputContainer>

        <Modal.Description>
          <Modal.DescriptionText>Description</Modal.DescriptionText>
          <Modal.Textarea
            value={description}
            rows={20}
            onChange={e => {
              setDescription(e.target.value);
            }}
          />
        </Modal.Description>

        <Modal.End>
          <Button onClick={handleSubmit}>Add</Button>
        </Modal.End>
      </Modal.Modal>
    </Modal.Overlay>
  );
};

export default AddBatchModal;
