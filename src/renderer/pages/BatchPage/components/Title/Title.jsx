import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'UI/Typography/Typography';
import IconButton from 'UI/IconButton/IconButton';
import { Complete, Break } from 'UI/Icons/Icons';
import { StyledTitle, TitleContainer } from 'Pages/TasksPage/components/Tasks/partials/Title/styles';
import useLoader from 'Common/Loader/useLoader';
import { validateRequest } from 'Utils/validateRequest';
import appStorage from 'Utils/AppStorage';
import { GetCurrentTasks, GetCompletedTasks, DeleteTask, UpdateTask } from 'Services/TaskService';
import { useAppDispatch } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import { GetBatches } from 'Services/BatchService';

const Title = props => {
  const dispatch = useAppDispatch();
  const { name, id, description, batchId, dueDate } = props;
  const { setActive } = useLoader();
  const token = appStorage.getItem('token');

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        let index = -1;
        batch.tasks.forEach(task => {
          index += 1;
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          if (task.status === 'completed') {
            batch.tasks.splice(index, 1);
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const resetCurrentTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };

  const resetCompletedTasks = async () => {
    await validateRequest(GetCompletedTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_COMPLETED_TASKS,
        data: result,
      });
    });
  };

  const handleComplete = async () => {
    setActive(true);
    const data = new FormData();
    data.append('taskId', id);
    data.append('name', name);
    data.append('description', description);
    data.append('status', 'completed');
    data.append('batchId', batchId);
    data.append('dueDate', '');
    try {
      await validateRequest(UpdateTask(token, data), () => {
        resetBatches();
        resetCurrentTasks();
        resetCompletedTasks();
        dispatch({
          type: types.UPDATE_TASK,
        });
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  const handleSubmit = async () => {
    setActive(true);
    const data = new FormData();
    data.append('taskId', id);
    data.append('name', name);
    data.append('description', description);
    data.append('status', 'current');
    data.append('batchId', '');
    data.append('dueDate', dueDate);
    try {
      await validateRequest(UpdateTask(token, data), () => {
        resetCurrentTasks();
        resetBatches();
        dispatch({
          type: types.UPDATE_TASK,
        });
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };
  return (
    <StyledTitle>
      <Typography variant="h2" fontSize="20px" fontFamily="bold">
        <TitleContainer>{name}</TitleContainer>
      </Typography>
      <div style={{ display: 'flex', width: '45px', 'justify-content': 'space-between' }}>
        <IconButton src={<Complete />} variant="success" width="20px" height="20px" onClick={handleComplete} />
        <IconButton src={<Break />} variant="danger" width="20px" height="20px" onClick={handleSubmit} />
      </div>
    </StyledTitle>
  );
};

Title.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  batchId: PropTypes.string.isRequired,
  dueDate: PropTypes.string.isRequired,
};

export default Title;
