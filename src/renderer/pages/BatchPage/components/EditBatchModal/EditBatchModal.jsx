import React, { useEffect, useState } from 'react';
import Input from 'UI/Input/Input';
import Button from 'UI/Button/Button';
import IconButton from 'UI/IconButton/IconButton';
import { Close } from 'UI/Icons/Icons';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import * as Modal from 'Common/Modal/styles';
import useLoader from 'Common/Loader/useLoader';
import appStorage from 'Utils/AppStorage';
import { validateRequest } from 'Utils/validateRequest';
import { GetBatches, UpdateBatch } from 'Services/BatchService';
import { GetCurrentTasks } from 'Services/TaskService';

const EditBatchModal = () => {
  const { open, batch } = useAppState().editBatchModalOpen;
  const { setActive } = useLoader();
  const dispatch = useAppDispatch();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const token = appStorage.getItem('token');
  useEffect(() => {
    setName(batch.name);
    setDescription(batch.description);
  }, [batch]);

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const resetTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };

  const handleSubmit = async () => {
    setActive(true);
    try {
      const data = new FormData();
      data.append('batchId', batch.id);
      data.append('name', name || batch.name);
      data.append('description', description);
      await validateRequest(UpdateBatch(token, data), () => {
        dispatch({
          type: types.OPEN_EDIT_BATCH_MODAL,
          status: false,
          data: { id: '', name: '', description: '', tasks: [] },
        });
        resetTasks();
        resetBatches();
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <Modal.Overlay open={open}>
      <Modal.Modal>
        <Modal.Title>
          <Modal.TitleText>Edit batch</Modal.TitleText>
          <Modal.TitleExit>
            <IconButton
              src={<Close />}
              width="24px"
              height="24px"
              onClick={() =>
                dispatch({
                  type: types.OPEN_EDIT_BATCH_MODAL,
                  status: false,
                  data: { id: '', name: '', description: '', dueDate: '', batchId: '', batchName: '' },
                })
              }
            />
          </Modal.TitleExit>
        </Modal.Title>

        <Modal.InputContainer>
          <Input
            value={name}
            label="Title"
            onChange={e => {
              setName(e.target.value);
            }}
          />
        </Modal.InputContainer>

        <Modal.Description>
          <Modal.DescriptionText>Description</Modal.DescriptionText>
          <Modal.Textarea
            value={description}
            rows={20}
            onChange={e => {
              setDescription(e.target.value);
            }}
          />
        </Modal.Description>

        <Modal.End>
          <Button onClick={handleSubmit}>Edit</Button>
        </Modal.End>
      </Modal.Modal>
    </Modal.Overlay>
  );
};

export default EditBatchModal;
