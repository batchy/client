/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from 'react';
import PropTypes from 'prop-types';
import Button from 'UI/Button/Button';
import IconButton from 'UI/IconButton/IconButton';
import { Break, Close } from 'UI/Icons/Icons';
import * as Modal from 'Common/Modal/styles';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import { StyledEnd } from 'Pages/BatchPage/components/SetBatchModal/styles';
import { StyledSelect, StyledSelectContainer } from 'Pages/BatchPage/styles';
import Typography from 'UI/Typography/Typography';
import { formatDate } from 'Utils/formatDate';
import { validateRequest } from 'Utils/validateRequest';
import { GetCurrentTasks, UpdateTask } from 'Services/TaskService';
import useLoader from 'Common/Loader/useLoader';
import appStorage from 'Utils/AppStorage';
import { GetBatches } from 'Services/BatchService';

const SetBatchModal = props => {
  const { open, task } = useAppState().batchModalOpen;
  const { batches } = useAppState();
  const dispatch = useAppDispatch();
  const { setActive } = useLoader();

  const resetTasks = async () => {
    const token = appStorage.getItem('token');
    try {
      await validateRequest(GetCurrentTasks(token), result => {
        if (result === null) result = [];
        result.forEach(item => {
          if (item.subtasks === null) {
            item.subtasks = [];
          }
        });
        dispatch({
          type: types.SET_CURRENT_TASKS,
          data: result,
        });
      });
      await validateRequest(GetBatches(token), result => {
        if (result === null) result = [];
        result.forEach(batch => {
          if (batch.tasks === null) {
            batch.tasks = [];
          }
          batch.tasks.forEach(task => {
            if (task.subtasks === null) {
              task.subtasks = [];
            }
            task.batchName = batch.name;
            task.batchId = batch.id;
          });
        });
        dispatch({
          type: types.SET_BATCHES,
          data: result,
        });
        dispatch({
          type: types.OPEN_SET_BATCH_MODAL,
          status: false,
        });
      });
    } catch (err) {
      alert(err.message);
    }
  };

  const handleSubmit = async batchId => {
    const token = appStorage.getItem('token');
    const { id, name, description, dueDate } = task;
    setActive(true);
    try {
      const data = new FormData();
      data.append('taskId', id);
      data.append('name', name);
      data.append('description', description);
      data.append('dueDate', formatDate(dueDate).substring(0, dueDate.length - 1));
      data.append('status', 'current');
      data.append('batchId', batchId);
      await validateRequest(UpdateTask(token, data), () => {
        dispatch({
          type: types.OPEN_SET_BATCH_MODAL,
          status: false,
          data: { id: '', name: '', description: '', dueDate: '', batchId: '', batchName: '' },
        });
        resetTasks();
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <Modal.Overlay open={open}>
      <Modal.Modal>
        <Modal.Title>
          <Modal.TitleText>Assign task to batch</Modal.TitleText>
          <Modal.TitleExit>
            <IconButton
              src={<Close />}
              width="24px"
              height="24px"
              onClick={() =>
                dispatch({
                  type: types.OPEN_SET_BATCH_MODAL,
                  status: false,
                })
              }
            />
          </Modal.TitleExit>
        </Modal.Title>

        <StyledEnd>
          {batches.length ? (
            batches.map(batch => {
              return (
                <Button
                  key={batch.id}
                  onClick={() => {
                    handleSubmit(batch.id);
                  }}
                >
                  {batch.name}
                </Button>
              );
            })
          ) : (
            <Typography variant="span" fontSize="24px" fontFamily="bold" color="c_Accent">
              <div style={{ margin: '16px' }}>No batches here :(</div>
            </Typography>
          )}
        </StyledEnd>
      </Modal.Modal>
    </Modal.Overlay>
  );
};

SetBatchModal.defaultProps = {
  open: true,
};

export default SetBatchModal;
