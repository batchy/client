/* eslint-disable @typescript-eslint/explicit-function-return-type */
import styled from 'styled-components';
import { StyledIcon } from 'UI/Icon/styles';
import { StyledButton } from 'UI/Button/styles';

export const StyledEnd = styled.div`
  width: 446px;

  margin: ${props => props.theme.offsets.sm};
  box-sizing: border-box;

  align-items: center;

  display: flex;
  flex-direction: column;

  ${StyledButton} {
    width: 446px;
    margin-bottom: 5px;
  }
`;
