import styled from 'styled-components';
import { StyledButton } from 'UI/Button/styles';

export const Container = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: #faf6ff;
`;

export const CreateButton = styled(StyledButton)`
  width: 550px;
  display: flex;
  background: #fcf9ff;
  border: 2px solid #a491ff;
  box-sizing: border-box;
  border-radius: 5px;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.25);
  background: #ffffff;
  margin: 85px 0px 9px 0px;
`;

export const Backdrop = styled.div`
  position: relative;
  width: 550px;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 11px 0px 32px 0px;
  padding: 0px 14px 9px 16px;
  border-radius: 8px;
  box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.25);
  background: #ffffff;
  overflow-y: scroll;
  overflow-x: hidden;

  ::-webkit-scrollbar-track {
    background-color: ${props => props.theme.colors.c_Primary};
    border-radius: 8px;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: ${props => props.theme.colors.c_Accent_active};
  }

  ::-webkit-scrollbar-thumb:hover {
    background-color: ${props => props.theme.colors.c_Accent};
  }

  ::-webkit-scrollbar {
    width: 7px;
  }
`;

export const StyledSelectContainer = styled.div`
  width: 514px;
  height: 28.27px;
  margin: 13px 0px 15px 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;

export const StyledSelect = styled.select`
  max-width: 400px;
  height: 28.27px;
  font-family: ${props => props.theme.fonts.bold};
  font-size: 24px;
  justify-self: self-start;
  border: none;
  color: ${props => props.theme.colors.c_Accent};
  :focus {
    outline: none;
  }
`;

export const StyledDescription = styled.div`
  width: 514px;
  word-wrap: break-word;
  margin: 0px 0px 15px 0px;
  padding: 0px 0px 0px 5px;
`;
