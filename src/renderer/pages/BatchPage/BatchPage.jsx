import React, { useEffect, useState } from 'react';
import Navbar from 'Common/Navbar/Navbar';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import Typography from 'UI/Typography/Typography';
import IconButton from 'UI/IconButton/IconButton';
import { Break, Edit } from 'UI/Icons/Icons';
import { StyledTask } from 'Pages/TasksPage/styles';
import useLoader from 'Common/Loader/useLoader';
import appStorage from 'Utils/AppStorage';
import { validateRequest } from 'Utils/validateRequest';
import { DeleteBatch, GetBatches } from 'Services/BatchService';
import Subtask from 'Pages/TasksPage/components/Tasks/partials/Subtask/Subtask';
import NewSubtask from 'Pages/TasksPage/components/Tasks/partials/NewSubtask/NewSubtask';
import DateBadge from 'Pages/TasksPage/components/Tasks/partials/DateBadge/DateBadge';
import { GetCurrentTasks } from 'Services/TaskService';
import { Backdrop, Container, CreateButton, StyledSelectContainer, StyledSelect, StyledDescription } from './styles';
import Title from './components/Title/Title';
import AddBatchModal from './components/AddBatchModal/AddBatchModal';
import EditBatchModal from './components/EditBatchModal/EditBatchModal';

const BatchPage = () => {
  const { isAuthed, batches } = useAppState();
  const dispatch = useAppDispatch();
  const { setActive } = useLoader();
  const [currentBatchId, setCurrentBatchId] = useState('');
  const [currentBatch, setCurrentBatch] = useState({ id: '', name: '', description: '', tasks: [] });
  const token = appStorage.getItem('token');

  const openAddBatchModal = () => {
    dispatch({
      type: types.OPEN_ADD_BATCH_MODAL,
      status: true,
    });
  };

  const resetTasks = async () => {
    const token = appStorage.getItem('token');
    try {
      await validateRequest(GetCurrentTasks(token), result => {
        if (result === null) result = [];
        result.forEach(item => {
          if (item.subtasks === null) {
            item.subtasks = [];
          }
        });
        dispatch({
          type: types.SET_CURRENT_TASKS,
          data: result,
        });
      });
    } catch (err) {
      alert(err.message);
    }
  };

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const handleDelete = async () => {
    setActive(true);
    const data = new FormData();
    data.append('batchId', currentBatchId);
    try {
      await validateRequest(DeleteBatch(token, data), () => {
        resetBatches();
        resetTasks();
        setCurrentBatchId('');
        setCurrentBatch('', '', '', []);
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  useEffect(() => {
    if (batches.length && currentBatchId === '') {
      setCurrentBatchId(batches[0].id);
    }
  }, [batches]);

  useEffect(() => {
    batches.forEach(batch => {
      if (batch.id === currentBatchId) {
        setCurrentBatch(batch);
      }
    });
  }, [currentBatchId, batches]);

  useEffect(() => {
    if (!isAuthed) {
      window.location.hash = '#/';
    }
  }, [isAuthed]);

  return (
    <Container>
      <Navbar />
      <CreateButton onClick={openAddBatchModal}>Add new batch</CreateButton>
      <Backdrop>
        {batches.length ? (
          <StyledSelectContainer>
            <StyledSelect
              value={currentBatchId}
              onChange={e => {
                setCurrentBatchId(e.target.value);
              }}
            >
              {batches.map(batch => {
                return (
                  <option key={batch.id} value={batch.id}>
                    {batch.name}
                  </option>
                );
              })}
            </StyledSelect>
            <div style={{ display: 'flex', width: '45px', 'justify-content': 'space-between' }}>
              <IconButton
                src={<Edit />}
                variant="primary"
                width="20px"
                height="20px"
                onClick={() =>
                  dispatch({
                    type: types.OPEN_EDIT_BATCH_MODAL,
                    status: true,
                    data: currentBatch,
                  })
                }
              />
              <IconButton src={<Break />} variant="danger" width="20px" height="20px" onClick={handleDelete} />
            </div>
          </StyledSelectContainer>
        ) : (
          <Typography variant="span" fontSize="24px" fontFamily="bold" color="c_Accent">
            <div style={{ margin: '16px' }}>No batches =(</div>
          </Typography>
        )}
        <Typography variant="p" fontSize="18px" fontFamily="bold">
          <StyledDescription>{currentBatch.description ? currentBatch.description : ''}</StyledDescription>
        </Typography>
        {currentBatch.tasks
          ? currentBatch.tasks.map(task => {
              return (
                <StyledTask key={task.id}>
                  <Title
                    name={task.name}
                    batchId={task.batchId}
                    description={task.description}
                    id={task.id}
                    status={task.status}
                  />
                  <Typography variant="p" fontSize="14px" fontFamily="bold">
                    <StyledDescription style={{ width: '472px' }}>{task.description}</StyledDescription>
                  </Typography>
                  {task.subtasks
                    ? task.subtasks.map(subtask => {
                        return <Subtask id={subtask.id} key={subtask.id} name={subtask.body} />;
                      })
                    : ''}
                  <NewSubtask id={task.id} />
                  {task.dueDate && (
                    <div style={{ display: 'flex' }}>
                      <DateBadge
                        dueDate={task.dueDate}
                        name={task.name}
                        taskId={task.id}
                        batchId={task.batchId}
                        descrtption={task.description}
                      />
                    </div>
                  )}
                </StyledTask>
              );
            })
          : ''}
      </Backdrop>
      <AddBatchModal />
      <EditBatchModal />
    </Container>
  );
};

export default BatchPage;
