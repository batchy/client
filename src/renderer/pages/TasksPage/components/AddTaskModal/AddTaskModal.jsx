/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Input from 'UI/Input/Input';
import Button from 'UI/Button/Button';
import IconButton from 'UI/IconButton/IconButton';
import { Close } from 'UI/Icons/Icons';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import * as Modal from 'Common/Modal/styles';
import useLoader from 'Common/Loader/useLoader';
import { validateRequest } from 'Utils/validateRequest';
import appStorage from 'Utils/AppStorage';
import { formatDate } from 'Utils/formatDate';
import { CreateTask, GetCurrentTasks } from 'Services/TaskService';

const AddTaskModal = props => {
  const { addTaskModalOpen } = useAppState();
  const { setActive } = useLoader();
  const dispatch = useAppDispatch();
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [date, setDate] = useState('');

  const handleSubmit = async () => {
    setActive(true);
    const token = appStorage.getItem('token');
    const data = new FormData();
    data.append('name', name);
    data.append('description', description);
    data.append('dueDate', formatDate(date));
    setName('');
    setDescription('');
    setDate('');
    try {
      await validateRequest(CreateTask(token, data), () => {
        dispatch({
          type: types.OPEN_ADD_TASK_MODAL,
          status: false,
        });
      });
      await validateRequest(GetCurrentTasks(token), result => {
        if (result === null) result = [];
        result.forEach(item => {
          if (item.subtasks === null) {
            item.subtasks = [];
          }
        });
        dispatch({
          type: types.SET_CURRENT_TASKS,
          data: result,
        });
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <Modal.Overlay open={addTaskModalOpen}>
      <Modal.Modal>
        <Modal.Title>
          <Modal.TitleText>Add a new task</Modal.TitleText>
          <Modal.TitleExit>
            <IconButton
              src={<Close />}
              width="24px"
              height="24px"
              onClick={() =>
                dispatch({
                  type: types.OPEN_ADD_TASK_MODAL,
                  status: false,
                })
              }
            />
          </Modal.TitleExit>
        </Modal.Title>

        <Modal.InputContainer>
          <Input
            required
            value={name}
            label="Name"
            onChange={e => {
              setName(e.target.value);
            }}
          />
        </Modal.InputContainer>

        <Modal.Description>
          <Modal.DescriptionText>Description</Modal.DescriptionText>
          <Modal.Textarea
            value={description}
            rows={20}
            onChange={e => {
              setDescription(e.target.value);
            }}
          />
        </Modal.Description>

        <Modal.InputContainer>
          <Input value={date} label="Data" type="datetime-local" onChange={e => setDate(e.target.value)} />
        </Modal.InputContainer>

        <Modal.End>
          <Button onClick={() => handleSubmit()}>Add</Button>
        </Modal.End>
      </Modal.Modal>
    </Modal.Overlay>
  );
};

export default AddTaskModal;
