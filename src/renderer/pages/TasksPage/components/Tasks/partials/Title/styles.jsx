import styled from 'styled-components';

export const StyledTitle = styled.div`
  width: 472px;
  height: 28.27px;
  margin: 0px 0px 8px 0px;
  padding: 0px 6px 0px 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const TitleContainer = styled.div`
  width: 362px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
`;
