import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'UI/Typography/Typography';
import IconButton from 'UI/IconButton/IconButton';
import { Complete } from 'UI/Icons/Icons';
import { StyledSubtask } from 'Pages/TasksPage/components/Tasks/partials/Subtask/styles';
import useLoader from 'Common/Loader/useLoader';
import { DeleteSubtask, GetCurrentTasks } from 'Services/TaskService';
import appStorage from 'Utils/AppStorage';
import { validateRequest } from 'Utils/validateRequest';
import types from 'Utils/AppState/appStateTypes';
import { useAppDispatch } from 'Utils/AppState/AppState';
import { GetBatches } from 'Services/BatchService';

const Subtask = props => {
  const { name, id } = props;
  const dispatch = useAppDispatch();
  const { setActive } = useLoader();
  const token = appStorage.getItem('token');

  const resetTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const handleSubmit = async () => {
    setActive(true);
    const data = new FormData();
    data.append('taskId', id);
    try {
      await validateRequest(DeleteSubtask(token, data), () => {
        resetTasks();
        resetBatches();
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <StyledSubtask>
      <Typography variant="h3" fontSize="16px" fontFamily="bold">
        <div style={{ 'white-space': 'nowrap', overflow: 'hidden', 'text-overflow': 'ellipsis', width: '430px' }}>
          {name}
        </div>
      </Typography>
      <div style={{ display: 'flex', width: '25px', 'justify-content': 'center' }}>
        <IconButton src={<Complete />} variant="success" width="20px" height="20px" onClick={handleSubmit} />
      </div>
    </StyledSubtask>
  );
};

Subtask.propTypes = {
  name: PropTypes.string.isRequired,
  id: PropTypes.string.isRequired,
};

export default Subtask;
