import styled from 'styled-components';

export const StyledSubtask = styled.div`
  width: 472px;
  height: 36px;
  margin: 8px 0px 8px 0px;
  padding: 7px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  background: white;
`;