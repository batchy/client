import React from 'react';
import PropTypes from 'prop-types';
import IconButton from 'UI/IconButton/IconButton';
import { Delete } from 'UI/Icons/Icons';
import { StyledBatchBadge } from 'Pages/TasksPage/components/Tasks/partials/BatchBadge/styles';
import { useAppDispatch } from 'Utils/AppState/AppState';
import useLoader from 'Common/Loader/useLoader';
import appStorage from 'Utils/AppStorage';
import { validateRequest } from 'Utils/validateRequest';
import { GetBatches, UpdateBatch } from 'Services/BatchService';
import types from 'Utils/AppState/appStateTypes';
import { GetCurrentTasks, UpdateTask } from 'Services/TaskService';
import Typography from 'UI/Typography/Typography';

const BatchBadge = props => {
  const dispatch = useAppDispatch();
  const { dueDate, taskId, name, description, batchId, batchName } = props;
  const { setActive } = useLoader();
  const token = appStorage.getItem('token');

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const resetTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };

  const handleSubmit = async () => {
    setActive(true);
    const data = new FormData();
    data.append('taskId', taskId);
    data.append('name', name);
    data.append('description', description);
    data.append('status', 'current');
    data.append('batchId', '');
    data.append('dueDate', dueDate);
    try {
      await validateRequest(UpdateTask(token, data), () => {
        resetTasks();
        resetBatches();
        dispatch({
          type: types.UPDATE_TASK,
        });
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <StyledBatchBadge>
      <div style={{ margin: '0px 10px 0px 0px' }}>
        <Typography>{batchName}</Typography>
      </div>
      <div>
        <IconButton src={<Delete />} variant="danger" width="20px" height="20px" onClick={handleSubmit} />
      </div>
    </StyledBatchBadge>
  );
};

BatchBadge.propTypes = {
  dueDate: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  taskId: PropTypes.string.isRequired,
  batchId: PropTypes.string.isRequired,
  batchName: PropTypes.string.isRequired,
};

BatchBadge.defaultProps = {
  description: '',
};

export default BatchBadge;
