import React from 'react';
import PropTypes from 'prop-types';
import IconButton from 'UI/IconButton/IconButton';
import { Delete } from 'UI/Icons/Icons';
import { StyledDateBadge } from 'Pages/TasksPage/components/Tasks/partials/DateBadge/styles';
import Typography from 'UI/Typography/Typography';
import { validateRequest } from 'Utils/validateRequest';
import appStorage from 'Utils/AppStorage';
import useLoader from 'Common/Loader/useLoader';
import { UpdateTask, GetCurrentTasks } from 'Services/TaskService';
import { useAppDispatch } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import { GetBatches, UpdateBatch } from 'Services/BatchService';

const DateBadge = props => {
  const dispatch = useAppDispatch();
  const { dueDate, id, name, description, batchId } = props;
  const FormatedDate = new Date(Date.parse(dueDate));
  FormatedDate.setHours(FormatedDate.getHours() - 3);
  const { setActive } = useLoader();
  const token = appStorage.getItem('token');

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const resetTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };

  const handleSubmit = async () => {
    setActive(true);
    const data = new FormData();
    data.append('taskId', id);
    data.append('name', name);
    data.append('description', description);
    data.append('status', 'current');
    data.append('batchId', batchId);
    data.append('dueDate', '');
    try {
      await validateRequest(UpdateTask(token, data), () => {
        resetTasks();
        resetBatches();
        dispatch({
          type: types.UPDATE_TASK,
        });
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    dueDate !== '0001-01-01T00:00:00Z' && (
      <StyledDateBadge>
        <div style={{ margin: '0px 10px 0px 0px' }}>
          <Typography fontSize="12px">{FormatedDate.toLocaleString()}</Typography>
        </div>
        <div>
          <IconButton src={<Delete />} variant="danger" width="20px" height="20px" onClick={handleSubmit} />
        </div>
      </StyledDateBadge>
    )
  );
};

DateBadge.propTypes = {
  dueDate: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  taskId: PropTypes.string.isRequired,
  batchId: PropTypes.string,
};

export default DateBadge;
