import styled from 'styled-components';

export const StyledDateBadge = styled.div`
  height: 28px;
  padding: 3px 8px 3px 8px;
  margin: 7px 10px 0px 0px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  border-radius: 15px;
  background: white;
`;
