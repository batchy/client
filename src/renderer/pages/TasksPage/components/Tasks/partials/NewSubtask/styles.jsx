import styled from 'styled-components';

export const StyledInput = styled.input`
  position: absolute;
  width: 472px;
  height: 36px;
  padding: 7px 27px 7px 7px;
  font-size: 16px;
  color: ${props => props.theme.colors.t_Primary};
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  background: white;
  border: none;
  outline: none;
  :hover {
    box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.3);
  }
`;

export const SubmitIconButton = styled.button`
  position: absolute;
  top: 9px;
  right: 8px;
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  background: white;
  border: none;
  outline: none;
`;

export const NewSubtaskContainer = styled.div`
  position: relative;
  width: 472px;
  height: 36px;
  margin: 8px 0px 8px 0px;
`;
