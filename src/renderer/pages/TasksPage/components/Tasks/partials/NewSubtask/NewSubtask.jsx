import React, { useState } from 'react';
import PropTypes from 'prop-types';
import IconButton from 'UI/IconButton/IconButton';
import { Add } from 'UI/Icons/Icons';
import {
  NewSubtaskContainer,
  StyledInput,
  SubmitIconButton,
} from 'Pages/TasksPage/components/Tasks/partials/NewSubtask/styles';
import appStorage from 'Utils/AppStorage';
import useLoader from 'Common/Loader/useLoader';
import { validateRequest } from 'Utils/validateRequest';
import { CreateSubtask, GetCurrentTasks } from 'Services/TaskService';
import types from 'Utils/AppState/appStateTypes';
import { useAppDispatch } from 'Utils/AppState/AppState';
import { GetBatches } from 'Services/BatchService';

const NewSubtask = props => {
  const { id } = props;
  const dispatch = useAppDispatch();
  const { setActive } = useLoader();
  const [description, setDescription] = useState('');
  const token = appStorage.getItem('token');
  const resetTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const handleSubmit = async () => {
    setActive(true);

    const data = new FormData();
    data.append('body', description);
    setDescription('');
    try {
      await validateRequest(CreateSubtask(token, data, id), () => {
        resetTasks();
        resetBatches();
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };
  return (
    <NewSubtaskContainer>
      <StyledInput
        placeholder="New subtask..."
        value={description}
        onChange={e => {
          setDescription(e.target.value);
        }}
      />
      <SubmitIconButton type="button">
        <IconButton src={<Add />} variant="primary" width="20px" height="20px" onClick={handleSubmit} />
      </SubmitIconButton>
    </NewSubtaskContainer>
  );
};

NewSubtask.propTypes = {
  id: PropTypes.string.isRequired,
};

export default NewSubtask;
