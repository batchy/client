import styled from 'styled-components';

export const StyledDescription = styled.div`
  width: 472px;
  word-wrap: break-word;
`;
