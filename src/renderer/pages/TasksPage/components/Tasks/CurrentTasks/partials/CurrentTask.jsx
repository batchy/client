import React from 'react';
import PropTypes from 'prop-types';
import Typography from 'UI/Typography/Typography';
import { useAppDispatch } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import Title from '../../partials/Title/Title';
import Subtask from '../../partials/Subtask/Subtask';
import NewSubtask from '../../partials/NewSubtask/NewSubtask';
import DateBadge from '../../partials/DateBadge/DateBadge';
import BatchBadge from '../../partials/BatchBadge/BatchBadge';
import { StyledTask } from '../../../../styles';
import { StyledDescription } from './styles';

const CurrentTask = props => {
  const dispatch = useAppDispatch();
  const { task } = props;
  const { name, description, subtasks, dueDate, batchId, batchName, id } = task;

  return (
    <StyledTask>
      <Title
        name={name}
        batchId={batchId}
        description={description}
        id={task.id}
        setEditTaskModalOpen={() =>
          dispatch({
            type: types.OPEN_EDIT_TASK_MODAL,
            status: true,
            data: task,
          })
        }
        setBatchModalOpen={() =>
          dispatch({
            type: types.OPEN_SET_BATCH_MODAL,
            status: true,
            data: task,
          })
        }
      />
      <Typography variant="p" fontSize="14px" fontFamily="bold">
        <StyledDescription>{description}</StyledDescription>
      </Typography>
      {subtasks.map(subtask => {
        return <Subtask id={subtask.id} key={subtask.id} name={subtask.body} />;
      })}
      <NewSubtask id={id} />
      <div style={{ display: 'flex', width: '472px' }}>
        {dueDate && (
          <DateBadge dueDate={dueDate} id={task.id} description={description} name={name} batchId={batchId} />
        )}
        {batchId && batchName && (
          <BatchBadge
            dueDate={dueDate}
            taskId={task.id}
            description={description}
            name={name}
            batchName={batchName}
            batchId={batchId}
          />
        )}
      </div>
    </StyledTask>
  );
};

CurrentTask.propTypes = {
  task: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    subtasks: PropTypes.arrayOf(PropTypes.object),
    dueDate: PropTypes.string,
    batchId: PropTypes.string,
    batchName: PropTypes.string,
  }).isRequired,
};

export default CurrentTask;
