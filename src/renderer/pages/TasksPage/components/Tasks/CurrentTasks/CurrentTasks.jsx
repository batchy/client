import React from 'react';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import CurrentTask from 'Pages/TasksPage/components/Tasks/CurrentTasks/partials/CurrentTask';
import { CreateButton } from 'Pages/TasksPage/styles';
import types from 'Utils/AppState/appStateTypes';
import Typography from 'UI/Typography/Typography';

const CurrentTasks = () => {
  const { currentTasks: tasks } = useAppState();
  const dispatch = useAppDispatch();

  const openAddTaskModal = () => {
    dispatch({
      type: types.OPEN_ADD_TASK_MODAL,
      status: true,
    });
  };

  return (
    <>
      <CreateButton onClick={openAddTaskModal}>Add a new task</CreateButton>
      {tasks.length ? (
        tasks.map(task => {
          return <CurrentTask key={task.id} task={task} />;
        })
      ) : (
        <Typography variant="span" fontSize="24px" fontFamily="bold" color="c_Accent">
          <div style={{ margin: '16px' }}>No tasks yet =(</div>
        </Typography>
      )}
    </>
  );
};

export default CurrentTasks;
