import React from 'react';
import * as PropTypes from 'prop-types';
import Typography from 'UI/Typography/Typography';
import IconButton from 'UI/IconButton/IconButton';
import { Delete, Restore } from 'UI/Icons/Icons';
import useLoader from 'Common/Loader/useLoader';
import { validateRequest } from 'Utils/validateRequest';
import { DeleteTask, GetCompletedTasks, GetCurrentTasks, UpdateTask } from 'Services/TaskService';
import appStorage from 'Utils/AppStorage';
import types from 'Utils/AppState/appStateTypes';
import { useAppDispatch } from 'Utils/AppState/AppState';
import { GetBatches } from 'Services/BatchService';
import { StyledTitle } from '../../partials/Title/styles';
import { StyledSubtask } from '../../partials/Subtask/styles';
import { StyledTask } from '../../../../styles';
import { StyledDescription } from '../../CurrentTasks/partials/styles';

const CompletedTask = props => {
  const { id, name, description, subtasks, batchId } = props;
  const { setActive } = useLoader();
  const dispatch = useAppDispatch();
  const token = appStorage.getItem('token');
  const resetCompletedTasks = async () => {
    await validateRequest(GetCompletedTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_COMPLETED_TASKS,
        data: result,
      });
    });
  };
  const resetCurrentTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };
  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };
  const handleSubmit = async () => {
    setActive(true);
    const data = new FormData();
    data.append('taskId', id);
    data.append('name', name);
    data.append('description', description);
    data.append('status', 'current');
    data.append('batchId', batchId);
    data.append('dueDate', '');
    try {
      await validateRequest(UpdateTask(token, data), () => {
        resetCompletedTasks();
        resetCurrentTasks();
        resetBatches();
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  const handleDelete = async () => {
    setActive(true);
    const data = new FormData();
    data.append('taskId', id);
    try {
      await validateRequest(DeleteTask(token, data), () => {
        resetCompletedTasks();
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };
  return (
    <StyledTask key={id}>
      <StyledTitle>
        <Typography variant="h2" fontSize="20px" fontFamily="bold">
          <div style={{ 'white-space': 'nowrap', overflow: 'hidden', 'text-overflow': 'ellipsis', width: '412px' }}>
            {name}
          </div>
        </Typography>
        <div style={{ display: 'flex', width: '45px', 'justify-content': 'space-between' }}>
          <IconButton src={<Restore />} variant="primary" width="20px" height="20px" onClick={handleSubmit} />
          <IconButton src={<Delete />} variant="danger" width="20px" height="20px" onClick={handleDelete} />
        </div>
      </StyledTitle>
      <Typography variant="p" fontSize="12px" fontFamily="bold">
        <StyledDescription>{description}</StyledDescription>
      </Typography>
      {subtasks.map(subtask => {
        return (
          <StyledSubtask key={subtask.id}>
            <Typography variant="h3" fontSize="16px" fontFamily="bold">
              <div style={{ 'white-space': 'nowrap', overflow: 'hidden', 'text-overflow': 'ellipsis', width: '460px' }}>
                {subtask.body}
              </div>
            </Typography>
          </StyledSubtask>
        );
      })}
    </StyledTask>
  );
};

CompletedTask.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  subtasks: PropTypes.arrayOf(PropTypes.object),
  batchId: PropTypes.string,
};

CompletedTask.defaultProps = {
  description: '',
  batchId: '',
  subtasks: [],
};

export default CompletedTask;
