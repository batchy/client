import React from 'react';
import { useAppState } from 'Utils/AppState/AppState';
import Typography from 'UI/Typography/Typography';
import CompletedTask from './partials/CompletedTask';

const CompletedTasks = () => {
  const { completedTasks: tasks } = useAppState();

  return (
    <>
      {tasks.length ? (
        tasks.map(task => {
          return (
            <CompletedTask
              key={task.id}
              id={task.id}
              name={task.name}
              description={task.description}
              subtasks={task.subtasks}
              batchId={task.batchId}
            />
          );
        })
      ) : (
        <Typography variant="span" fontSize="24px" fontFamily="bold" color="c_Accent">
          <div style={{ margin: '16px' }}>No completed tasks yet, get to work =)</div>
        </Typography>
      )}
    </>
  );
};

export default CompletedTasks;
