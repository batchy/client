/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React, { useEffect, useState } from 'react';
import Input from 'UI/Input/Input';
import Button from 'UI/Button/Button';
import IconButton from 'UI/IconButton/IconButton';
import { Close } from 'UI/Icons/Icons';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import types from 'Utils/AppState/appStateTypes';
import * as Modal from 'Common/Modal/styles';
import appStorage from 'Utils/AppStorage';
import { GetCurrentTasks, UpdateTask } from 'Services/TaskService';
import useLoader from 'Common/Loader/useLoader';
import { validateRequest } from 'Utils/validateRequest';
import { SignIn } from 'Services/AuthService';
import { formatDate } from 'Utils/formatDate';
import { GetBatches } from 'Services/BatchService';

const EditTaskModal = () => {
  const { setActive } = useLoader();
  const { open, task } = useAppState().editTaskModal;
  const dispatch = useAppDispatch();
  const token = appStorage.getItem('token');
  const [name, setName] = useState('');
  const [description, setDescription] = useState('');
  const [date, setDate] = useState('');
  useEffect(() => {
    setName(task.name);
    setDescription(task.description);
    if (task.dueDate !== '0001-01-01T00:00:00Z') setDate(task.dueDate.substr(0, task.dueDate.length - 4));
    else setDate('');
  }, [task]);

  const resetBatches = async () => {
    await validateRequest(GetBatches(token), result => {
      if (result === null) result = [];
      result.forEach(batch => {
        if (batch.tasks === null) {
          batch.tasks = [];
        }
        batch.tasks.forEach(task => {
          if (task.subtasks === null) {
            task.subtasks = [];
          }
          task.batchName = batch.name;
          task.batchId = batch.id;
        });
      });
      dispatch({
        type: types.SET_BATCHES,
        data: result,
      });
    });
  };

  const resetTasks = async () => {
    await validateRequest(GetCurrentTasks(token), result => {
      if (result === null) result = [];
      result.forEach(item => {
        if (item.subtasks === null) {
          item.subtasks = [];
        }
      });
      dispatch({
        type: types.SET_CURRENT_TASKS,
        data: result,
      });
    });
  };

  const handleSubmit = async () => {
    setActive(true);
    try {
      const data = new FormData();
      data.append('taskId', task.id);
      data.append('name', name || task.name);
      data.append('description', description);
      data.append('status', 'current');
      data.append('batchId', task.batchId);
      data.append('dueDate', date ? formatDate(date) : task.dueDate);
      await validateRequest(UpdateTask(token, data), () => {
        dispatch({
          type: types.OPEN_EDIT_TASK_MODAL,
          status: false,
          data: { id: '', name: '', description: '', dueDate: '', batchId: '', batchName: '' },
        });
        resetTasks();
        resetBatches();
      });
    } catch (err) {
      alert(err.message);
    }
    setActive(false);
  };

  return (
    <Modal.Overlay open={open}>
      <Modal.Modal>
        <Modal.Title>
          <Modal.TitleText>Edit task</Modal.TitleText>
          <Modal.TitleExit>
            <IconButton
              src={<Close />}
              width="24px"
              height="24px"
              onClick={() =>
                dispatch({
                  type: types.OPEN_EDIT_TASK_MODAL,
                  status: false,
                  data: { id: '', name: '', description: '', dueDate: '', batchId: '', batchName: '' },
                })
              }
            />
          </Modal.TitleExit>
        </Modal.Title>

        <Modal.InputContainer>
          <Input label="Name" value={name} onChange={e => setName(e.target.value)} />
        </Modal.InputContainer>

        <Modal.Description>
          <Modal.DescriptionText fontFamily="regular">Description</Modal.DescriptionText>
          <Modal.Textarea rows={20} value={description} onChange={e => setDescription(e.target.value)} />
        </Modal.Description>

        <Modal.InputContainer>
          <Input label="Date" value={date} type="datetime-local" onChange={e => setDate(e.target.value)} />
        </Modal.InputContainer>

        <Modal.End>
          <Button onClick={handleSubmit}>Edit</Button>
        </Modal.End>
      </Modal.Modal>
    </Modal.Overlay>
  );
};

export default EditTaskModal;
