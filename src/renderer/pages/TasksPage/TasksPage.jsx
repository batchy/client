import React, { useEffect, useState } from 'react';
import Navbar from 'Common/Navbar/Navbar';
import Typography from 'UI/Typography/Typography';
import { useAppDispatch, useAppState } from 'Utils/AppState/AppState';
import CompletedTasks from 'Pages/TasksPage/components/Tasks/CompletedTasks/CompletedTasks';
import { Container, Backdrop, Controls, CurrentCompletedContainer, CreateButton } from './styles';
import CurrentTasks from './components/Tasks/CurrentTasks/CurrentTasks';
import AddTaskModal from './components/AddTaskModal/AddTaskModal';
import EditTaskModal from './components/EditTaskModal/EditTaskModal';
import SetBatchModal from '../BatchPage/components/SetBatchModal/SetBatchModal';

const TasksPage = () => {
  const [AddTaskModalOpen, setAddTaskModalOpen] = useState(false);
  const [showCompleted, setShowCompleted] = useState(false);

  const { isAuthed } = useAppState();
  const dispatch = useAppDispatch();

  useEffect(() => {
    if (!isAuthed) {
      window.location.hash = '#/';
    }
  }, [isAuthed]);

  return (
    <Container>
      <Navbar />
      <Backdrop>
        <Controls>
          <CurrentCompletedContainer>
            <Typography
              variant="span"
              fontSize="24px"
              fontFamily="bold"
              color={!showCompleted ? 'c_Accent' : 'rgba(164, 145, 255, 0.25)'}
              onClick={() => setShowCompleted(false)}
              style={{ userSelect: 'none' }}
            >
              Current
            </Typography>
            <Typography variant="h2" fontSize="24px" fontFamily="bold" color="c_Accent">
              /
            </Typography>
            <Typography
              variant="span"
              fontSize="24px"
              fontFamily="bold"
              color={showCompleted ? 'c_Accent' : 'rgba(164, 145, 255, 0.25)'}
              onClick={() => setShowCompleted(true)}
              style={{ userSelect: 'none' }}
            >
              Completed
            </Typography>
          </CurrentCompletedContainer>
        </Controls>
        {!showCompleted ? <CurrentTasks /> : <CompletedTasks />}
      </Backdrop>
      <AddTaskModal />
      <EditTaskModal />
      <SetBatchModal />
    </Container>
  );
};

export default TasksPage;
