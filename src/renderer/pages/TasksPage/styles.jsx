import styled from 'styled-components';
import { StyledButton } from 'UI/Button/styles';

export const Container = styled.div`
  position: absolute;
  width: 100vw;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: #faf6ff;
`;

export const Backdrop = styled.div`
  position: relative;
  width: 550px;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: 85px 0px 32px 0px;
  padding: 0px 0px 9px 0px;
  border-radius: 8px;
  box-shadow: 0px 0px 25px rgba(0, 0, 0, 0.25);
  background: #ffffff;
  overflow-y: scroll;
  overflow-x: hidden;

  ::-webkit-scrollbar-track {
    background-color: ${props => props.theme.colors.c_Primary};
    border-radius: 8px;
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: ${props => props.theme.colors.c_Accent_active};
  }

  ::-webkit-scrollbar-thumb:hover {
    background-color: ${props => props.theme.colors.c_Accent};
  }

  ::-webkit-scrollbar {
    width: 7px;
  }
`;

export const Controls = styled.div`
  position: sticky;
  width: 504px;
  height: 34px;
  display: flex;
  justify-content: center;
  margin: 9px 16px 12px 16px;
`;

export const CurrentCompletedContainer = styled.div`
  width: 298px;
  height: 34px;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
`;

export const CreateButton = styled(StyledButton)`
  width: 504px;
  background: #fcf9ff;
  border: 2px solid #a491ff;
  box-sizing: border-box;
  border-radius: 5px;
  margin: 0px 0px 9px 0px;
`;

export const StyledTask = styled.div`
  width: 504px;
  height: auto;
  margin: 9px 16px 9px 16px;
  background: #faf6ff;
  box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.1);
  border-radius: 8px;
  padding: 11px 16px 19px 16px;
`;
