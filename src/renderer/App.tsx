import * as React from 'react';
import { ThemeProvider } from 'styled-components';
import { Switch, Route } from 'react-router-dom';
import theme from 'Theme/theme';
import HomePage from 'Pages/HomePage/HomePage';
import { LoaderProvider } from 'Common/Loader/LoaderContext';
import { AppStateProvider } from 'Utils/AppState/AppState';
import TasksPage from 'Pages/TasksPage/TasksPage';
import BatchPage from 'Pages/BatchPage/BatchPage';

const App: React.FunctionComponent = () => {
  return (
    <ThemeProvider theme={theme}>
      <LoaderProvider>
        <AppStateProvider>
          <Switch>
            <Route exact path="/">
              <HomePage />
            </Route>
            <Route exact path="/tasks">
              <TasksPage />
            </Route>
            <Route exact path="/batches">
              <BatchPage />
            </Route>
          </Switch>
        </AppStateProvider>
      </LoaderProvider>
    </ThemeProvider>
  );
};

export default App;
