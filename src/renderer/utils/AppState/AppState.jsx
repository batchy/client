import React, { createContext, useContext, useEffect, useReducer } from 'react';
import * as PropTypes from 'prop-types';
import appStorage from 'Utils/AppStorage';
import appStateReducer from './appStateReducer';

export const AppStateContext = createContext({});
export const AppDispatchContext = createContext(() => {});

export const useAppState = () => useContext(AppStateContext);
export const useAppDispatch = () => useContext(AppDispatchContext);

export const AppStateProvider = props => {
  const { children } = props;
  const initialState = {
    userData: {},
    isAuthed: false,
    batches: [],
    editTaskModal: {
      open: false,
      task: {
        id: '',
        name: '',
        description: '',
        dueDate: '',
        batchId: '',
        batchName: '',
      },
    },
    addBatchModalOpen: false,
    editBatchModalOpen: {
      open: false,
      batch: {
        id: '',
        name: '',
        description: '',
        tasks: [],
      },
    },
    addTaskModalOpen: false,
    batchModalOpen: {
      open: false,
      task: {
        id: '',
        name: '',
        description: '',
        dueDate: '',
        batchId: '',
        batchName: '',
      },
    },
    completedTasks: [],
    currentTasks: [],
  };

  const [state, dispatch] = useReducer(appStateReducer, initialState);

  return (
    <AppStateContext.Provider value={state}>
      <AppDispatchContext.Provider value={dispatch}>{children}</AppDispatchContext.Provider>
    </AppStateContext.Provider>
  );
};

AppStateProvider.propTypes = {
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]).isRequired,
};
