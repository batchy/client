import types from 'Utils/AppState/appStateTypes';

const appStateReducer = (state, action) => {
  switch (action.type) {
    case types.SET_BATCHES:
      if (!action.data) {
        throw new Error('No action data provided. Check your dispatcher usages.');
      }
      return {
        ...state,
        batches: action.data,
      };
    case types.SET_CURRENT_TASKS:
      if (!action.data) {
        throw new Error('No action data provided. Check your dispatcher usages.');
      }
      return {
        ...state,
        currentTasks: action.data,
      };
    case types.SET_COMPLETED_TASKS:
      if (!action.data) {
        throw new Error('No action data provided. Check your dispatcher usages.');
      }
      return {
        ...state,
        completedTasks: action.data,
      };
    case types.SET_USER_DATA:
      if (!action.data) {
        throw new Error('No action data provided. Check your dispatcher usages.');
      }
      return {
        ...state,
        userData: action.data,
      };
    case types.SET_USER_AUTH_STATUS:
      return {
        ...state,
        isAuthed: action.status,
      };
    case types.OPEN_ADD_TASK_MODAL:
      return {
        ...state,
        addTaskModalOpen: action.status,
      };
    case types.OPEN_EDIT_TASK_MODAL:
      return {
        ...state,
        editTaskModal: {
          open: action.status,
          task: action.data,
        },
      };
    case types.OPEN_EDIT_BATCH_MODAL:
      return {
        ...state,
        editBatchModalOpen: {
          open: action.status,
          batch: action.data,
        },
      };
    case types.OPEN_SET_BATCH_MODAL:
      return {
        ...state,
        batchModalOpen: {
          open: action.status,
          task: action.data,
        },
      };
    case types.OPEN_ADD_BATCH_MODAL:
      return {
        ...state,
        addBatchModalOpen: action.status,
      };
    case types.DELETE_TASK:
      return {
        ...state,
      };
    case types.UPDATE_TASK:
      return {
        ...state,
      };
    default:
      throw new Error(`No reducer defined for type ${action.type}`);
  }
};

export default appStateReducer;
