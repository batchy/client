// formatDate accepts a string of type YYYY-MM-DDTHH:MM and attaches :00 to the end
export const formatDate = date => {
  return `${date}:00`;
};
