import electron from 'electron';
import path from 'path';
import fs from 'fs';

interface AppStorage {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  init: Function;
  getItem: Function;
  setItem: Function;
}

interface Config {
  defaults: {
    dimensions: { height: number; width: number };
  };
}

const filePath = (): string => {
  const appDataPath = (electron.app || electron.remote.app).getPath('userData');
  return path.join(appDataPath, `appData.json`);
};

const appStorage: AppStorage = {
  init(config: Config): void {
    try {
      fs.readFileSync(filePath()).toString();
    } catch (err) {
      fs.writeFileSync(filePath(), JSON.stringify(config.defaults));
    }
  },
  getItem<T>(key: string): T {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let fileData: Record<string, any>;
    try {
      fileData = JSON.parse(fs.readFileSync(filePath()).toString());
    } catch (err) {
      throw new Error(err.message);
    }
    return fileData[key];
  },
  setItem<T>(key: string, value: T): void {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let fileData: Record<string, any>;
    try {
      fileData = JSON.parse(fs.readFileSync(filePath()).toString());
    } catch (err) {
      throw new Error(err.message);
    }
    fileData[key] = value;
    fs.writeFileSync(filePath(), JSON.stringify(fileData));
  },
};

export default appStorage;
