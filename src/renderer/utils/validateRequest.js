// validateRequest validates a response :)
export const validateRequest = async (request, successCallback) => {
  const response = await request;
  const result = await response.json();
  switch (response.status) {
    // All good
    case 200:
      successCallback(result);
      break;
    case 400:
      throw new Error(result.message);
    // Failed to authenticate (bad token)
    case 401:
      throw new Error('Incorrect login or password.');
    // Unknown request route.
    case 404:
      throw new Error('Unknown request route');
    // Database failure, returns a Number and a Message.
    case 424:
      throw new Error(`${result.Number}: ${result.Message}`);
    // Server failure
    case 500:
      throw new Error(`${result.message}`);
    default:
      throw new Error('Oops! An unexpected error occurred.');
  }
};
