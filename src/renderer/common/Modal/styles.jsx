import styled from 'styled-components';
import { StyledIcon } from 'UI/Icon/styles';

export const Modal = styled.div`
  width: 478px;

  display: flex;
  flex-direction: column;

  background: #ffffff;
  border: 2px solid ${props => props.theme.colors.c_Accent};
  box-sizing: border-box;
  box-shadow: 0px 0px 20px rgba(0, 0, 0, 0.2);
  border-radius: 8px;
`;

export const Overlay = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 100vw;
  height: 100vh;

  display: flex;
  justify-content: center;
  align-items: center;

  background: rgba(246, 246, 246, 0.6);
  visibility: ${props => (props.open ? 'visible' : 'hidden')};
  opacity: ${props => (props.open ? 1 : 0)};

  transition: 0.5s visibility, 0.5s opacity ease;
`;

export const Title = styled.div`
  width: 446px;
  height: 34px;
  margin: ${props => props.theme.offsets.sm} ${props => props.theme.offsets.sm} 0 ${props => props.theme.offsets.sm};
  box-sizing: border-box;
  justify-content: space-between;
  align-items: center;

  display: flex;
`;

export const TitleText = styled.div`
  width: 300px;
  height: 34px;
  box-sizing: border-box;

  font-family: ${props => props.theme.fonts.bold};
  font-size: 24px;

  align-self: self-start;

  color: ${props => props.theme.colors.c_Accent};
`;

export const TitleExit = styled.div`
  width: 24px;
  height: 24px;
  box-sizing: border-box;

  align-self: self-end;
  
  ${StyledIcon} {
    fill: ${props => props.theme.colors.c_Accent};
  }
`;

export const InputContainer = styled.div`
  width: 446px;
  height: 64px;
  margin-top: 5px;
  margin-left: 16px;
  margin-right: 16px;
  box-sizing: border-box;

  display: flex;
  flex-direction: column;
`;

export const Description = styled.div`
  width: 446px;
  height: 222px;
  margin-top: 5px;
  margin-left: 16px;
  margin-right: 16px;

  box-sizing: border-box;
  display: flex;
  flex-direction: column;
`;

export const DescriptionText = styled.div`
  width: 102px;
  height: 28px;

  font-family: Nunito;
  font-style: normal;
  font-weight: normal;
  font-family: ${props => props.theme.fonts.regular};
  font-size: 20px;
  display: flex;
  align-items: center;

  color: ${props => props.theme.colors.t_Primary};
`;

export const Textarea = styled.textarea`
  width: 446px;
  height: auto;

  background: ${props => props.theme.colors.c_Primary_hover};

  border: 3px solid ${props => props.theme.colors.c_Accent_active};
  box-sizing: border-box;
  border-radius: 5px;

  padding: ${props => props.theme.offsets.xs};
  font-family: ${props => props.theme.fonts.regular};
  font-size: 16px;

  outline: none;
  transition: border 0.2s ease;
  will-change: border;

  resize: none;

  ::-webkit-scrollbar-track {
    background-color: ${props => props.theme.colors.c_Primary};
  }

  ::-webkit-scrollbar-thumb {
    border-radius: 10px;
    background-color: ${props => props.theme.colors.c_Accent};
  }

  ::-webkit-scrollbar {
    width: 7px;
  }

`;

export const End = styled.div`
  width: 446px;
  height: 34px;
  margin: ${props => props.theme.offsets.sm};
  box-sizing: border-box;
  justify-content: flex-end;
  align-items: center;

  display: flex;
`;
