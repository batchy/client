/* eslint-disable @typescript-eslint/explicit-function-return-type */
import styled from 'styled-components';
import { StyledIcon } from 'UI/Icon/styles';

export const StyledFab = styled.div`
  position: fixed;
  bottom: 64px;
  right: 64px;

  width: 56px;
  height: 56px;

  display: flex;
  justify-content: center;
  align-items: center;

  background: ${props => props.theme.colors.c_Accent};
  border-radius: 50%;
  border: 2px solid ${props => props.theme.colors.c_Accent};
  box-sizing: border-box;
  box-shadow: 2px 4px 8px rgba(0, 0, 0, 0.1);

  &:hover {
    background: ${props => props.theme.colors.c_Primary};
  }
  &:active {
    background: ${props => props.theme.colors.c_Accent_hover};
  }

  ${StyledIcon} {
    fill: ${props => props.theme.colors.c_Primary};
  }
  ${StyledIcon}:hover {
    fill: ${props => props.theme.colors.c_Accent};
  }
`;
