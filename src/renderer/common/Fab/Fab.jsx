/* eslint-disable react/prop-types */
/* eslint-disable @typescript-eslint/explicit-function-return-type */
import React from 'react';
import Icon from 'UI/Icon/Icon';
import { StyledFab } from './styles';

const Fab = props => {
  const { onClick, src } = props;
  return (
    <StyledFab onClick={onClick}>
      <Icon src={src} width="40px" height="40px" />
    </StyledFab>
  );
};

export default Fab;
