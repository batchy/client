import React, { createContext, useState } from 'react';
import * as PropTypes from 'prop-types';
import Loader from './Loader';

export const LoaderContext = createContext({});

export const LoaderProvider = props => {
  const { children } = props;
  const [active, setActive] = useState(false);
  return (
    <LoaderContext.Provider value={{ setActive }}>
      {children}
      <Loader active={active} />
    </LoaderContext.Provider>
  );
};

LoaderProvider.propTypes = {
  children: PropTypes.node.isRequired,
};
