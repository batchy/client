import { useContext } from 'react';
import { LoaderContext } from './LoaderContext';

const useLoader = () => useContext(LoaderContext);

export default useLoader;
