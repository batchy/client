/* eslint-disable @typescript-eslint/explicit-function-return-type */
import styled from 'styled-components';

export const StyledOverlay = styled.div`
  //positioning
  position: absolute;
  left: 0;
  top: 0;
  // dimensions
  width: 100vw;
  height: 100vh;

  display: flex;
  justify-content: center; // main axis
  align-items: center; // cross axis

  background: rgba(246, 246, 246, 0.6);
  visibility: ${props => (props.active ? 'visible' : 'hidden')};
  opacity: ${props => (props.active ? 1 : 0)};

  transition: 0.5s visibility, 0.5s opacity ease;
`;

export const StyledSpinner = styled.div`
  border: 4px solid ${props => props.theme.colors.c_Accent_hover}; /* Light grey */
  border-top: 4px solid ${props => props.theme.colors.c_Accent}; /* Blue */
  border-radius: 50%;
  width: 64px;
  height: 64px;
  animation: spin 0.75s ease infinite;

  @keyframes spin {
    0% {
      transform: rotate(0deg);
    }
    100% {
      transform: rotate(360deg);
    }
  }
`;
