import React from 'react';
import * as PropTypes from 'prop-types';
import { StyledOverlay, StyledSpinner } from './styles';

const Loader = props => {
  const { active } = props;
  return (
    <StyledOverlay active={active}>
      <StyledSpinner />
    </StyledOverlay>
  );
};

Loader.propTypes = {
  active: PropTypes.bool,
};

Loader.defaultProps = {
  active: false,
};

export default Loader;
