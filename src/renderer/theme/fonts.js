const fonts = {
  regular: 'Nunito-Regular',
  bold: 'Nunito-Bold',
  italic: 'Nunito-Italic',
};

export default fonts;
