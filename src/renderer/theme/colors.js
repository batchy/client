const colors = {
  c_Primary: '#F5ECFF',
  c_Primary_hover: '#F5ECFF25',
  c_Primary_active: '#F5ECFF40',
  c_Accent: '#A491FF',
  c_Accent_hover: '#A491FF25',
  c_Accent_active: '#A491FF40',
  c_Success: '#2bb94a',
  c_Success_hover: '#2bb94a25',
  c_Success_active: '#2bb94a40',
  c_Danger: '#f85058',
  c_Danger_hover: '#f8505825',
  c_Danger_active: '#f8505840',
  t_Primary: '#141313',
  t_Secondary: '#F5F5F5',
};

export default colors;
