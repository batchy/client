import colors from './colors';
import fonts from './fonts';
import offsets from './offsets';

const theme = {
  colors,
  fonts,
  offsets,
};

export default theme;
