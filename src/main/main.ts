/* eslint-disable @typescript-eslint/no-var-requires,import/no-extraneous-dependencies,global-require */
import { app, BrowserWindow } from 'electron';
import appStorage from 'Utils/AppStorage';
import installExtension, { REACT_DEVELOPER_TOOLS } from 'electron-devtools-installer';

installExtension(REACT_DEVELOPER_TOOLS)
  .then(name => {
    console.log(`Added Extension:  ${name}`);
  })
  .catch(err => {
    console.log('An error occurred: ', err);
  });
// eslint-disable-next-line @typescript-eslint/no-explicit-any
declare let MAIN_WINDOW_WEBPACK_ENTRY: any;

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) {
  // eslint-disable-line global-require
  app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
// eslint-disable-next-line @typescript-eslint/no-explicit-any,import/prefer-default-export
let mainWindow: any;

const createWindow = async (): Promise<void> => {
  // Initialize appStorage.
  appStorage.init({
    defaults: {
      dimensions: {
        height: 800,
        width: 600,
      },
    },
  });
  // Grab previous height and width from appStorage.
  const { width, height } = appStorage.getItem('dimensions');
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width,
    height,
    // titleBarStyle: 'hiddenInset',
    webPreferences: {
      preload: MAIN_WINDOW_WEBPACK_ENTRY,
      nodeIntegration: true, // insecure, find another solution ASAP
    },
  });

  // and load the index.html of the app.
  await mainWindow.loadURL(MAIN_WINDOW_WEBPACK_ENTRY);

  // Open the DevTools.
  // mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', (): void => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });

  mainWindow.on('resize', (): void => {
    const { height, width } = mainWindow.getBounds();
    appStorage.setItem('dimensions', { height, width });
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
